# WebApp1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.

## Prerequisite 
- nodejs 18.12.1
- npm: 8.x.x
- Angular CLI: 15.0.0

## Development mode

- Configure your Oauth2 in environment.ts (issuer, clientId, scope...)

- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Test mode
- In test mode, we're using Oauth endpoint from https://idsvr4.azurewebsites.net
- run with command: ng serve --configuration=test
## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

## Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

## Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

## To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command
## Reference](https://angular.io/cli) page.
