import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './components/home/home.component';
import { ManagementComponent } from './components/management/management.component';
import { TaskComponent } from './components/task/task.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { WELCOME_COMPONENT_ROLE_CODE, HOME_COMPONENT_ROLE_CODE, TASK_COMPONENT_ROLE_CODE, MANAGEMENT_COMPONENET_ROLE_CODE  } from 'src/app/services/oauth2.service';

const routes: Routes = [
  { path: "welcome", component: WelcomeComponent},

  { path: "home", component: HomeComponent, canActivate: [AuthGuard],
    data: {
      componentCode: HOME_COMPONENT_ROLE_CODE
    }},
  { path: "task", component: TaskComponent, canActivate: [AuthGuard],
    data: {
      componentCode: TASK_COMPONENT_ROLE_CODE
  }},
  { path: "management", component: ManagementComponent, canActivate: [AuthGuard], 
  data: {
    componentCode: MANAGEMENT_COMPONENET_ROLE_CODE
  }},
  { path: "", redirectTo: "welcome", pathMatch: "full"},
  { path: "**", redirectTo: "welcome", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
