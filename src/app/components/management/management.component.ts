import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { User } from 'src/app/data/schema/user';
import { UserService } from 'src/app/data/service/user.service';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit{
  users: User[] = [];
  constructor(private userService: UserService, private oauthService: OAuthService) {

  }
  ngOnInit(): void {
    this.userService.getAllUsers(this.oauthService.getAccessToken()).subscribe((response) => {
      this.users = response.data;
    })
  }


}
