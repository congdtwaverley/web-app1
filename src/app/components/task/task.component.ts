import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { OAuthService } from 'angular-oauth2-oidc';
import { Observable, switchMap } from 'rxjs';
import { Task } from 'src/app/data/schema/task';
import { TaskListResponse } from 'src/app/data/schema/task-list-response';
import { User } from 'src/app/data/schema/user';
import { TaskService } from 'src/app/data/service/task.service';
import { Oauth2Service } from 'src/app/services/oauth2.service';

declare var window: any;

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  addTaskModal: any;
  tasks: Task[] = [];
  task?: Task;
  private user: User | undefined;
  private isUpdate: boolean = false;
  userName: string = '';
  isCreateNewTask: boolean = false;

  taskForm = this.formBuilder.group({
    id: [''],
    title: ['', Validators.required],
    description: ['', Validators.required],
    status: ['CREATED', Validators.required],
  })

  constructor(private taskService: TaskService
    , private oauth2Service: Oauth2Service
    , private oauthService: OAuthService, private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.addTaskModal = new window.bootstrap.Modal(
      document.getElementById("addTaskModal")
    );
    
    this.getUserTasks().subscribe((response) => {
      this.tasks = response.data;
    });
  }

  openAddTaskModal() {
    this.isCreateNewTask = true;
    this.taskForm = this.formBuilder.group({
      id: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      status: ['CREATED', Validators.required],
    })
    this.addTaskModal.show();
  }

  closeAddTaskModal() {
    this.addTaskModal.close();
  }

  taskFormSubmit() {
    if (this.isCreateNewTask) {
      this.createNewTask();
    } else {
      this.updateTask();
    }
  }


  createNewTask() {
    console.log('save new task here');
    console.log(this.taskForm.controls.title.value);

    this.user = this.oauth2Service.getUser();
    const { title, description } = this.taskForm.value;

    if (this.user && title && description) {
      var newTask: Task = new Task('', this.user, title, description, '');
      this.taskService.createTask(newTask, this.oauthService.getAccessToken()).pipe(
        switchMap((response) => {
          return this.getUserTasks();
        })
      ).subscribe((rs) => {
        this.tasks = rs.data;
      })
    }
  }

  updateTask() {
    console.log('Update existing task here');
    console.log(this.taskForm.controls.title.value);

    this.user = this.oauth2Service.getUser();
    const { id, title, description } = this.taskForm.value;
   
    if (this.user && title && description) {
      var newTask: Task = new Task(id ? id : '',  this.user, title, description, 'UPDATED');

      this.taskService.updateTask(newTask, this.oauthService.getAccessToken()).pipe(
        switchMap((response) => {
          return this.getUserTasks();
        })
      ).subscribe((rs) => {
        this.tasks = rs.data;
      })
    }
  }

  getUserTasks(): Observable<TaskListResponse>{
    this.userName = this.oauth2Service.getUser()?.userName || '';
    return this.taskService.getUserTasks(this.userName, this.oauthService.getAccessToken());
  }

  openEditForm(task: Task) {
    //Get task by Id from BE
    this.isCreateNewTask = false;
    this.taskForm.setValue({
      id: task.id,
      title: task.title,
      description: task.description,
      status: task.status
   })
    this.addTaskModal.show();
  }

  deleteTask(task: Task) {
    console.log('Delete task here ');

    this.taskService.deleteTask(task, this.oauthService.getAccessToken()).pipe(
      switchMap((response) => {
        return this.getUserTasks();
      })
    ).subscribe((rs) => {
      this.tasks = rs.data;
    })
  }
}
