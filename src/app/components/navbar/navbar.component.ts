import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Oauth2Service, WELCOME_COMPONENT_ROLE_CODE, HOME_COMPONENT_ROLE_CODE, TASK_COMPONENT_ROLE_CODE, MANAGEMENT_COMPONENET_ROLE_CODE  } from 'src/app/services/oauth2.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit{
  
  constructor (private oauth2Service: Oauth2Service) {}

  ngOnInit(): void {
    // this.configureSingleSignOn();
  }

  login() {
    this.oauth2Service.login();
  }

  logout() {
    // console.log('Logout URL:' + this.oauthService.logoutUrl)
    this.oauth2Service.logout();
  }

  get hasLoggedIn(): boolean {
    return this.oauth2Service.hasLoggedIn();
  }

  isHomeComponentAccesssible(): boolean {
    return this.oauth2Service.isComponentAccesssible(HOME_COMPONENT_ROLE_CODE);
  }
  isTaskComponentAccessible(): boolean {
    return this.oauth2Service.isComponentAccesssible(TASK_COMPONENT_ROLE_CODE);
  }
  isManagementComponentAccessible(): boolean {
    return this.oauth2Service.isComponentAccesssible(MANAGEMENT_COMPONENET_ROLE_CODE);
  }

}
