import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable, window } from 'rxjs';
import { Task } from '../schema/task';
import { TaskListResponse } from '../schema/task-list-response';
import { TaskResponse } from '../schema/task-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  
  rootUrl: string = environment.backendUrlConfig.rootURL;

  constructor(private httpClient: HttpClient) { }


  // public getTasksByUserName(userName: )

  public createTask(task: Task, accessToken: string): Observable<TaskResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/task';
    return this.httpClient.post<TaskResponse>(url, task, HTTP_OPTIONS);
  }

  public updateTask(task: Task, accessToken: string): Observable<TaskResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/task';
    return this.httpClient.put<TaskResponse>(url, task, HTTP_OPTIONS);
  }

  public deleteTask(task: Task, accessToken: string): Observable<TaskResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/task/' + task.id;
    return this.httpClient.delete<TaskResponse>(url, HTTP_OPTIONS);
  }

  public getUserTasks(userName: any, accessToken: string): Observable<TaskListResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/user/' + userName + '/task';
    return this.httpClient.get<TaskListResponse>(url, HTTP_OPTIONS);
  }
}
