import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../schema/user';
import { UserListResponse } from '../schema/user-list-response';
import { UserResponse } from '../schema/user-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  rootUrl: string = environment.backendUrlConfig.rootURL;

  constructor(private httpClient: HttpClient) { }

  public getUserByUserName(userName: string, accessToken: string): Observable<UserResponse> {
    var queryParams = new HttpParams();
    queryParams = queryParams.append("userName", userName);

    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      }),
      params: queryParams
    };
    environment.backendUrlConfig.rootURL;
    const url = this.rootUrl + '/api/v1/user/search';
    return this.httpClient.get<UserResponse>(url, HTTP_OPTIONS);
  }

  public getAllUsers(accessToken: string): Observable<UserListResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/user/';
    return this.httpClient.get<UserListResponse>(url, HTTP_OPTIONS);
  }

  public createMonoUser(user: User, accessToken: string): Observable<UserResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/user';
    return this.httpClient.post<UserResponse>(url, user, HTTP_OPTIONS);
  }

  public updateMonoUser(user: User, accessToken: string): Observable<UserResponse> {
    const HTTP_OPTIONS = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken
      })
    };
    const url = this.rootUrl + '/api/v1/user';
    return this.httpClient.put<UserResponse>(url, user, HTTP_OPTIONS);
  }

}
