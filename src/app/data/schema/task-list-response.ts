import { Task } from "./task";

export class TaskListResponse {
    data: Task[];

    constructor(data: Task[]) {
      this.data = data;
    }
}