import { Task } from "./task";

export class TaskResponse {
    data: Task;

    constructor(data: Task) {
      this.data = data;
    }
}