import { User } from "./user";

export class Task {
    id: string;
    monoUser: User;
    title: string;
    description: string;
    status: string;
    
    constructor(id: string, monoUser: User, title: string, description: string, status: string) {
        this.id = id;
        this.monoUser = monoUser;
        this.title = title;
        this.description = description;
        this.status = status;
    }
  }