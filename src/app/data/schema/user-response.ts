import { User } from "./user";

export class UserResponse {
    data: User

    constructor(data: User) {
      this.data = data;
    }
}