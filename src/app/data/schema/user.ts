export class User {
    id: string;
    authId: string;
    userName: string;
    firstName: string;
    lastName: string;
    email: string;
    status: string;

    constructor(id: string, authId: string, userName: string, firstName: string, lastName: string, email: string, status: string) {
      this.id = id;
      this.authId = authId;
      this.userName = userName;
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
      this.status = status;
    }
  }