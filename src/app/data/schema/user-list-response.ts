import { User } from "./user";

export class UserListResponse {
    data: User[];

    constructor(data: User[]) {
      this.data = data;
    }
}