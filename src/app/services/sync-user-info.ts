export interface UserInfo {
    isSynced: boolean;
    id: string;
    userName: string;
    nameOfUser: string;
    userEmail: string;
    firstName: string;
    lastName: string;
}