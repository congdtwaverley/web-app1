export const environment = {
    production: false,
    oauthconfig: {
        issuer: "http://localhost:8080/realms/WaverleyEco",
        redirectUri: "http://localhost:4200",
        clientId: "web-app1",
        scope: "profile email offline_access phone",
        showDebugInformation: true,
        requireHttps: false,
        postLogoutRedirectUri: "http://localhost:4200"
    },
    backendUrlConfig: {
        rootURL: "http://localhost:8081"
    }
}
